/**
 * Basic JSON response for controlleres
*/
export type BasicResponse = {
  message: string
}

/**
 * Error JSON response for controlleres
*/
export type ErrorResponse = {
  error: string,
  message: string
}
