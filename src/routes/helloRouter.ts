import { BasicResponse } from '../controller/types'
import express, { Request, Response } from 'express'
import { HelloController } from '../controller/HelloController'
import { LogInfo } from '../utils/logger'

// Router from express
const helloRouter = express.Router()

// GET -> http://localhost:8000/api/hello
helloRouter.route('/')
  // GET:
  .get(async (req: Request, res: Response) => {
    // obtain a query param
    const name: any = req?.query?.name
    LogInfo(`Query param: ${name}`)
    // Controller Instance o execute method
    const controller: HelloController = new HelloController()
    // Obtain response
    const response: BasicResponse = await controller.getMessage(name)
    // Send to the client the response
    return res.send(response)
  })

// export hello router
export default helloRouter
