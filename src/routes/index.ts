/*
 * Root router
 * Redirections to routers
*/

import express, { Request, Response } from 'express'
import helloRouter from './helloRouter'
import { LogInfo } from '../utils/logger'

// Server Instance
const server = express()

// Router Instance
const rootRouter = express.Router()

rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: http://localhost:8000/api/')
  res.send('Welcome to my API resful: Express + TS + Nodemon + Jest + Swagger + Mongoose')
})

// Redirections to routers & controllers
server.use('/', rootRouter) // GET -> http://localhost:8000/api/
server.use('/hello', helloRouter) // GET -> http://localhost:8000/api/hello

export default server
