import { userEntity } from '../entities/User.entity'
import { LogSuccess, LogError } from '@/utils/logger'

// CRUD

/**
 * Method to obtain all users from Collection "Users" in mongo server
*/
export const GetAllUsers = async () => {
  try {
    const userModel = userEntity()
    // Search all users
    return await userModel.find({ isDelete: false })
  } catch (err) {
    LogError(`[ORM ERROR]: Getting all users: ${err}`)
  }
}
