import express, { Express, Request, Response } from 'express'

// security
import cors from 'cors'
import helmet from 'helmet'

// TODO https

// Root routes
import rootRouter from '../routes'

// Create express server
const server: Express = express()

// Define SERVER to use "/api" and use rootRouter from 'index.ts' in routes
server.use('/api', rootRouter)

// Static server
server.use(express.static('public'))

// TODO Mongoose connection

// Security config
server.use(helmet())
server.use(cors())

// Content type config
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' }))

// redirection config
// http://localhost:8000/ --> http://localhost:8000/api
server.get('/', (req: Request, res: Response) => {
  res.redirect('/api')
})

export default server
